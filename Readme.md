This repository holds a script that adds a command to get the mouse position in a [Fungus](http://fungusgames.com/) flowchart.

![GetMousePosition screenshot](https://bitbucket.org/pierrerossel/getmouseposition/raw/master/Promotional%20assets/Screenshots/Flowchart.png =600x)

Install from Unity Asset Store:

  * Install [GetMousePosition command for Fungus](https://www.assetstore.unity3d.com/#!/content/117948) from the Asset Store


Install from this repository:

  * Visit the [downloads](https://bitbucket.org/pierrerossel/getmouseposition/downloads/) section 
  * Get the latest unitipackage. 
  * Import in your Unity project


Usage:

  * In a Fungus flowchart
  * Add a float, int or Vector3 variable to receive the mouse position
  * Select a block in the flowchart, add a command, look in the Input menu
  * Select the Get Mouse Position command or type its name in the search box
  * Configure the command, select the variable that will receive the mouse position
  
See the file [Readme.txt](https://bitbucket.org/pierrerossel/getmouseposition/src/master/Assets/GetMousePosition/Readme.txt) for more details on usage.